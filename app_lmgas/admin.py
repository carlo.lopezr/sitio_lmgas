from django.contrib import admin
from .models import Hora_despacho, Marca, MetodoPago, MiCarrito, Producto, TipoProd,Pedido,Estado,MetodoPago,Region,Direccion,Detallepedido

# Register your models here.
admin.site.register(Marca)
admin.site.register(TipoProd)
admin.site.register(Producto)
admin.site.register(Estado)
admin.site.register(Pedido)
admin.site.register(Detallepedido)
admin.site.register(MetodoPago)
admin.site.register(Hora_despacho)
admin.site.register(Region)
admin.site.register(Direccion)
admin.site.register(MiCarrito)
