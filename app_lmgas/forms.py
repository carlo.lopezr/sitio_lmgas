from django import forms
from django.core.checks.messages import Error
from django.db.models.base import Model
from django.forms import fields,ValidationError
from django.forms.models import ModelForm
from django.forms.utils import ErrorList
from .models import Direccion, MiCarrito, Producto,Pedido,Estado
from django.contrib.auth.forms import UserCreationForm, UserModel, UsernameField
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from django.contrib.admin.widgets import AdminDateWidget
from django.forms.fields import DateField



class FrmProducto(forms.ModelForm):
    class Meta:
        model=Producto
        fields=["id","marca","tipo","valor","stock","descripcion","estado","es_destacado","imagen"]


class frmCrearUsuario(UserCreationForm): 
    
    class Meta:
        model=User
        fields=["username","first_name","last_name","email","password1","password2"]
        labels={
            'username':_('Usuario'),
            'last_name':_('Apellido'),
        }
              

class frmEditarCliente(forms.ModelForm):
    class Meta:
        model=User
        fields=["first_name","last_name","email","is_active"]
        help_texts = {
            'is_active': _('Desmarque para deshabilitar a un cliente'),
        }

class frmEditarEstado(forms.ModelForm):
    class Meta:
        model=Pedido
        fields=["estado_pedido","hora_despacho","comentario"]

class frmModificarDatos(forms.ModelForm):
    class Meta:
        model=User
        fields=["first_name","last_name","email"]

class frmDatosDespacho(forms.ModelForm):

    # dia = forms.DateField(widget=forms.SelectDateWidget(years=range(2021,2022),months=range(1,2)))
    class Meta:
        model=Pedido
        fields=["retira","dia","hora_despacho","id_metodo","id_direccion"]  
        labels={
            'retira':_('Nombre y apellido de quien retira'),
            'dia':_('Seleccione dia para despacho'),
            'hora_despacho':_('Seleccione la hora de despacho'),
            'id_direccion':_('Seleccione dirección de despacho'),
            'id_metodo':_('Seleccione Método de pago'),
        }

class frmDireccion(forms.ModelForm):
    class Meta:
        model=Direccion
        fields=["alias","id_region","numero","calle","comuna","telefono"]     
