# Generated by Django 3.2.4 on 2021-06-28 18:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_lmgas', '0010_auto_20210628_1340'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='DetallePedido',
            new_name='Detalle_Pedido',
        ),
        migrations.AddField(
            model_name='pedido',
            name='comentario',
            field=models.CharField(default='Sin comentario', max_length=300),
        ),
    ]
