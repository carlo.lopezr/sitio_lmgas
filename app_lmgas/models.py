from django.db import models
from django.db.models.aggregates import Min
from django.db.models.deletion import CASCADE, PROTECT
from django.db.models.fields.related import ForeignKey
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User

# Create your models here.
class Marca(models.Model):
    id=models.IntegerField(primary_key=True)
    nombre=models.CharField(max_length=20)

    def __str__(self):
        return self.nombre

class Region(models.Model):
    id_region=models.IntegerField(primary_key=True)
    descripcion=models.CharField(max_length=100)

    def __str__(self):
        return self.descripcion

class Direccion(models.Model):
    id_direccion=models.AutoField(primary_key=True)
    id_cliente=models.ForeignKey(User,on_delete=PROTECT)
    id_region=models.ForeignKey(Region,on_delete=PROTECT)
    numero=models.IntegerField()
    calle=models.CharField(max_length=150)
    comuna=models.CharField(max_length=150)
    telefono=models.CharField(max_length=30,default="No especifica",null=True)
    alias=models.CharField(max_length=30, null=True)

    def __str__(self):
        return self.calle + " " + str(self.numero) + " " + self.comuna

class TipoProd(models.Model):
    id=models.IntegerField(primary_key=True)
    descripcion=models.CharField(max_length=50) 

    def __str__(self):
        return self.descripcion   

class Producto(models.Model):
    id=models.IntegerField(primary_key=True)
    marca=models.ForeignKey(Marca,on_delete=PROTECT)
    tipo=models.ForeignKey(TipoProd,on_delete=PROTECT)
    valor=models.IntegerField(default=0)
    stock=models.IntegerField(default=0)
    descripcion=models.CharField(max_length=50, null=True)
    imagen=models.ImageField(upload_to="imgprod",null=True)
    estado=models.BooleanField(default=True)
    es_destacado=models.BooleanField(default=False)

    def __str__(self):
        return str(self.id) + " " + self.descripcion + " " + str(self.marca)

    class Meta:
        ordering = ['id']

class MetodoPago(models.Model):
    id_metodo=models.IntegerField(primary_key=True)
    desc_metodo=models.CharField(max_length=150)

    def __str__(self):
        return self.desc_metodo

class Hora_despacho(models.Model):
    id_hora=models.IntegerField(primary_key=True)
    descripcion=models.CharField(max_length=150)

    def __str__(self):
        return self.descripcion

class Estado(models.Model):
    id_estado=models.IntegerField(primary_key=True)
    desc_estado=models.CharField(max_length=20)

    def __str__(self):
        return self.desc_estado

class Pedido(models.Model):
    id_pedido=models.AutoField(primary_key=True)
    id_metodo=models.ForeignKey(MetodoPago,on_delete=PROTECT,default=1)
    rut=models.ForeignKey(User,on_delete=PROTECT)
    id_direccion=models.ForeignKey(Direccion,default=1,on_delete=PROTECT,null=True,blank=True)
    estado_pedido=models.ForeignKey(Estado,on_delete=PROTECT,default=1)
    fec_pedido=models.DateField(auto_now_add=True)
    valor_despacho=models.IntegerField(default=0)
    dia=models.DateField(null=True)
    hora_despacho=models.ForeignKey(Hora_despacho,on_delete=PROTECT,default=1)
    complete=models.BooleanField(default=False)
    comentario=models.CharField(max_length=300,default="Sin comentario")
    retira=models.CharField(max_length=150,null=True)
    activo=models.BooleanField(default=True)

    class Meta:
        ordering = ['id_pedido']

    def __str__(self):
        return str(self.id_pedido)

    def obtenerTotal(self):
        detalles = Detallepedido.objects.filter(id_pedido=self.id_pedido)
        total=0
        for detalle in detalles:
            total += detalle.obtenerTotal()
        return total    
                
    def obtenerTotalFinal(self):
        totalfinal = (self.obtenerTotal()+self.valor_despacho)
        return totalfinal          

class Detallepedido(models.Model):
    id_pedido=models.ForeignKey(Pedido,on_delete=CASCADE)
    id_producto=models.ForeignKey(Producto,on_delete=PROTECT)
    cantidad=models.IntegerField(default=0, null=True, blank=True)
    date_added=models.DateField(auto_now_add=True)

    def __str__(self):
        return str(self.id_pedido) + " " + str(self.id_producto.descripcion)

    def obtenerTotal(self):
        return int(self.cantidad*self.id_producto.valor)  

class MiCarrito(models.Model):
    usuario = models.CharField(max_length=150,null=False)
    id_producto = models.ForeignKey(Producto,on_delete=PROTECT,null=True,blank=True)
    cantidad = models.IntegerField(null=True,blank=True)

    def obtenerTotal(self):
        return int(self.cantidad*self.id_producto.valor) 

  



    

