from rest_framework import serializers
from .models import Producto

class srlzProducto(serializers.ModelSerializer):
    class Meta:
        model=Producto
        fields=["id","valor","stock","descripcion","marca","tipo","es_destacado","imagen"]