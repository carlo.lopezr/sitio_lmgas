
var email1 = false;
var rut1 = false;
var telefono1 = false;

/* Validación del rut*/
function checkRut(rut) {
    // Despejar Puntos
    var valor = rut.value.replace('.', '');
    // Despejar Guión
    valor = valor.replace('-', '');

    // Aislar Cuerpo y Dígito Verificador
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();

    // Formatear RUN
    rut.value = cuerpo + '-' + dv

    // Si no cumple con el mínimo ej. (n.nnn.nnn)
    if (cuerpo.length < 7) {
        rut.setCustomValidity("RUT Incompleto");
        return false;
    }

    // Calcular Dígito Verificador
    suma = 0;
    multiplo = 2;

    // Para cada dígito del Cuerpo
    for (i = 1; i <= cuerpo.length; i++) {

        // Obtener su Producto con el Múltiplo Correspondiente
        index = multiplo * valor.charAt(cuerpo.length - i);

        // Sumar al Contador General
        suma = suma + index;

        // Consolidar Múltiplo dentro del rango [2,7]
        if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }

    }

    // Calcular Dígito Verificador en base al Módulo 11
    dvEsperado = 11 - (suma % 11);

    // Casos Especiales (0 y K)
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;

    // Validar que el Cuerpo coincide con su Dígito Verificador
    if (dvEsperado != dv) {
        /*rut.setCustomValidity("RUT Inválido");*/
        document.getElementById("hrut").innerHTML = "⛔Rut no válido"
        document.getElementById("hrut").className = "form-text text-danger";
        rut1 = false;
        return false;
    }
    else {
        document.getElementById("hrut").innerHTML = "✔ Rut Correcto";
        document.getElementById("hrut").className = "form-text text-success";
        rut1 = true;
        rut.setCustomValidity('');
    }

    // Si todo sale bien, eliminar errores (decretar que es válido)

}

/*Validar nombre*/
function sololetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = [8, 37, 39, 46];

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}

function validarEmail(email) {

    var texto = document.getElementById(email.id).value;
    var regex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

    if (texto.length > 0) {
        if (!regex.test(texto)) {
            document.getElementById("hmail").innerHTML = "⛔Email no válido";
            document.getElementById("hmail").className = "form-text text-danger";
            email1 = false;
        }
        else {
            document.getElementById("hmail").innerHTML = "✔ Email correcto";
            document.getElementById("hmail").className = "form-text text-success";
            email1 = true;
        }
    }
    else {
        document.getElementById("hmail").innerHTML = "";
    }


}

//Validar el largo de la contraseña
function validarContraseña(contraseña) {

    var contra = document.getElementById(contraseña.id).value;

    if (contra.length < 6) {
        document.getElementById("hcontra").innerHTML = "⛔La contraseña debe tener al menos 6 caracteres";
        document.getElementById("hcontra").className = "form-text text-danger";
        return false;
    }
    else if (contra.length > 25) {
        document.getElementById("hcontra").innerHTML = "⛔La contraseña no puede tener más de 25 caracteres";
        document.getElementById("hcontra").className = "form-text text-danger";
        return false;
    }
    else {
        document.getElementById("hcontra").innerHTML = "✔ Contraseña correcta";
        document.getElementById("hcontra").className = "form-text text-success";
        return true;
    }
}

function validarPassword() {

    var password1 = document.getElementById("pwd1").value;
    var password2 = document.getElementById("pwd2").value;

    if (password1 != password2) {
        document.getElementById("hcontra2").innerHTML = "⛔Las contraseñas no coinciden";
        document.getElementById("hcontra2").className = "form-text text-danger";
    }
    else {
        document.getElementById("hcontra2").innerHTML = "✔ Contraseña correcta";
        document.getElementById("hcontra2").className = "form-text text-success";
    }
}

function validar() {

    //Verificar nombre
    var nombre = document.getElementById("nombre").value;
    nombre = nombre.replace(/\s+/g, '');

    //Verificar apellido
    var apellido = document.getElementById("apellido").value;
    apellido = apellido.replace(/\s+/g, '');

    var p1 = document.getElementById("pwd1").value;
    p2 = document.getElementById("pwd2").value;
    var obj = document.getElementById("pwd2");
    var obj2 = document.getElementById("email");
    var obj3 = document.getElementById("rut");
    var obj4 = document.getElementById("nombre");
    var obj5 = document.getElementById("apellido");
    var obj6 = document.getElementById("pwd1");

    if (rut1 == false) {
        obj3.setCustomValidity('Ingrese un rut correcto');
    }
    else {
        obj3.setCustomValidity('');
    }

    if (nombre.length < 1) {
        obj4.setCustomValidity('Ingrese un nombre');
    }
    else {
        obj4.setCustomValidity('');
    }

    if (apellido < 1) {
        obj5.setCustomValidity('Ingrese un apellido');
    }
    else {
        obj5.setCustomValidity('');
    }

    if (email1 == false) {
        obj2.setCustomValidity('El email ingresado no es correcto');
    }
    else {
        obj2.setCustomValidity('');
    }

    if (p1.length < 6) {
        obj6.setCustomValidity('El largo de la contraseña no es correcto');
    }
    else {
        obj6.setCustomValidity('');
    }

    if (p1 != p2) {
        obj.setCustomValidity('Las contraseñas deben ser iguales');
    }

    else {
        obj.setCustomValidity('');
    } 
}

function validarTelefono() {

    var texto = document.getElementById(telefono.id).value;
    var regex = /^(\+?56)?(\s?)(0?9)(\s?)[9876543]\d{7}$/;

    if (texto.length > 0) {
        if (!regex.test(texto)) {
            document.getElementById("htelefono").innerHTML = "⛔Telefono no válido";
            document.getElementById("htelefono").className = "form-text text-danger";
            telefono1 = false;
        } else {
            document.getElementById("htelefono").innerHTML = "✔ Telefono correcto";
            document.getElementById("htelefono").className = "form-text text-success";
            telefono1 = true;
        }
    }
    else {
        document.getElementById("htelefono").innerHTML = "";
    }
}

function validarModificarDatos() {

    var nombre = document.getElementById("nombre").value;
    nombre = nombre.replace(/\s+/g, '');

    var apellido = document.getElementById("apellidos").value;
    apellido = apellido.replace(/\s+/g, '');

    var telefono = document.getElementById("telefono").value;
    telefono = telefono.replace(/\s+/g, '');

    var email = document.getElementById("email").value;

    var obj = document.getElementById("email");
    var obj2 = document.getElementById("nombre");
    var obj3 = document.getElementById("apellidos");
    var obj4 = document.getElementById("telefono");

    if (email.length < 1) {
        obj.setCustomValidity("Ingrese un email");
    }
    else {
        if (email1 == false) {
            obj.setCustomValidity('El email ingresado no es correcto');
        }
        else {
            obj.setCustomValidity('');
        }
    }

    if (nombre.length < 1) {
        obj2.setCustomValidity("Ingrese un nombre");
    }
    else {
        obj2.setCustomValidity('');
    }

    if (apellido < 1) {
        obj3.setCustomValidity('Ingrese sus apellidos');
    }
    else {
        obj3.setCustomValidity('');
    }

    if (telefono.length < 1) {
        telefono1 = true;
    }

    if (telefono1 == false) {
        obj4.setCustomValidity("El número de telefono ingresado no es correcto");
    }
    else {
        obj4.setCustomValidity('');
    }
}

function validarInicioSesion() {

    var rut = document.getElementById("rut").value;
    var contraseña = document.getElementById("contraseña").value;
    var obj = document.getElementById("rut");
    var obj2 = document.getElementById("contraseña");
    var obj3 = document.getElementById("rut");

    rut = rut.replace(/\s+/g, '');

    if (rut == "") {
        obj.setCustomValidity('Necesita ingresar su rut');
    }
    else {
        obj.setCustomValidity('');
    }

    if (contraseña == "") {
        obj2.setCustomValidity('Necesita ingresar una contraseña');
    }
    else {
        obj2.setCustomValidity('');
    }
}

function mostrar() {

    document.getElementById("agregarproducto").style.display = 'block';
    return false;

}

function ocultar(){
    document.getElementById("agregarproducto").style.display = 'none';
    return false;
}

function validarCambioContraseña(){
    var contraseñaActual = document.getElementById("pwd").value;
    var nuevaContraseña = document.getElementById("pwd1").value;
    var password = document.getElementById("pwd2").value;
    
    var obj = document.getElementById("pwd");
    var obj2 = document.getElementById("pwd1")
    var obj3 = document.getElementById("pwd2")

    if(contraseñaActual.length < 1){
        obj.setCustomValidity("Ingrese su contraseña actual");
    }
    else{
        obj.setCustomValidity('');
    }

    if(nuevaContraseña.length < 6){
        obj2.setCustomValidity("La contraseña debe tener al menos 6 caracteres");
    }
    else{
        obj2.setCustomValidity('');
    }

    if(password!=nuevaContraseña){
        obj3.setCustomValidity("Las contraseñas deben coincidir");
    }
    else{
        obj3.setCustomValidity('');
    }
}

function validarNombreRetira() { 
    var nombre = document.getElementById("nombre").value;

    var obj = document.getElementById("nombre");

    if(nombre.length < 1){
        obj.setCustomValidity("Debe ingresar nombre de quien retira");
    }
    else{
        obj.setCustomValidity('');
    }
 }

function editarProducto(){
    var marca = document.getElementById("marca").value;
    var tipo = document.getElementById("tipo").value;
    var valorProducto = document.getElementById("valorProducto").value;
    var stock = document.getElementById("stock").value;
    var descripcion = document.getElementById("descripcion").value;

    var obj = document.getElementById("marca");
    var obj2 = document.getElementById("tipo");
    var obj3 = document.getElementById("valorProducto");
    var obj4 = document.getElementById("stock");
    var obj5 = document.getElementById("descripcion");

    if(marca.length < 1){
        obj.setCustomValidity("Debe ingresar una marca");
    }
    else{
        obj.setCustomValidity('');
    }

    if(tipo.length < 1){
        obj2.setCustomValidity("Debe ingresar un tipo de producto");
    }
    else{
        obj2.setCustomValidity('');
    }

    if(valorProducto.length < 1){
        obj3.setCustomValidity("Debe ingresar un valor de producto");
    }
    else{
        obj3.setCustomValidity('');
    }

    if(stock.length < 1){
        obj4.setCustomValidity("Debe ingresar stock del producto");
    }
    else{
        obj4.setCustomValidity('');
    }

    if(descripcion.length < 1){
        obj5.setCustomValidity("Debe ingresar una descripcion del producto");
    }
    else{
        obj5.setCustomValidity('');
    }
}

