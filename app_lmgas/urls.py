from django.db import router
from django.urls import path, include
from .views import *
from rest_framework import routers

router=routers.DefaultRouter()
router.register('productos', ProductoViewSet)

urlpatterns = [
    path('', home,name="home"),
    path('Registro/',Registro,name="Registro"),
    path('AgregarProducto/<id>',AgregarProducto,name="AgregarProducto"),
    path('Admin/',Admin,name="Admin"),
    path('CambiarContraseña/', CambiarContraseña,name="CambiarContraseña"),
    path('AgregarProductoAdmin/',AgregarProductoAdmin,name="AgregarProductoAdmin"),
    path('Carrito/',Carrito,name="Carrito"),
    path('Cilindros/',Cilindros,name="Cilindros"),
    path('Contacto/',Contacto,name="Contacto"),
    path('RegistroExitoso/',RegistroExitoso,name="RegistroExitoso"),
    path('Despacho/',Despacho,name="Despacho"),
    path('ListaProductos/',ListaProductos,name="ListaProductos"),
    path('DetallePedido/<id>',DetallePedido,name="DetallePedido"),
    path('MisPedidos/',MisPedidos,name="MisPedidos"),
    path('EditarProducto/<id>',EditarProducto,name="EditarProducto"),
    path('EliminarProducto/<id>',EliminarProducto,name="EliminarProducto"),
    path('EliminarProductoSeguro/<id>',EliminarProductoSeguro,name="EliminarProductoSeguro"),
    path('AdminPedidos/',AdminPedidos,name="AdminPedidos"),
    path('Distribuidores/',Distribuidores,name="Distribuidores"),
    path('Nosotros/',Nosotros,name="Nosotros"),
    path('Estanques/',Estanques,name="Estanques"),
    path('MetodoDePago/',MetodoDePago,name="MetodoDePago"),
    path('FinalizarCompra/',FinalizarCompra,name="FinalizarCompra"),
    path('AdminClientes/',AdminClientes,name="AdminClientes"),
    path('EditarPedido/<id>',EditarPedido,name="EditarPedido"),  
    path('EditarCliente/<id>',EditarCliente,name="EditarCliente"),
    path('MiCuenta',MiCuenta,name="MiCuenta"),
    path('ModificarDatos/<id>',ModificarDatos,name="ModificarDatos"),  
    path('AgregarCarrito/<id>',AgregarCarrito,name="AgregarCarrito"),
    path('DisminuirCarrito/<id>',DisminuirCarrito,name="DisminuirCarrito"),
    path('AumentarCarrito/<id>',AumentarCarrito,name="AumentarCarrito"),
    path('TerminarCompra/',TerminarCompra,name="TerminarCompra"),  
    path('api/',include(router.urls)),  
    path('EliminarCarrito/<id>',EliminarCarrito,name="EliminarCarrito"),
    path('AgregarDireccion/',AgregarDireccion,name="AgregarDireccion"),
    path('EditarDireccion/<id>',EditarDireccion,name="EditarDireccion"),
    path('ModificarDatosAdmin/<id>',ModificarDatosAdmin,name="ModificarDatosAdmin"),
    path('EliminarDireccion/<id>',EliminarDireccion,name="EliminarDireccion"),
    path('AgregarDireccion2/',AgregarDireccion2,name="AgregarDireccion2"),
    path('Gracias/',Gracias,name="Gracias"),
]
