from django.contrib.auth.models import User
from django.contrib.messages.api import add_message
from django.core.exceptions import ObjectDoesNotExist
from .forms import FrmProducto,frmCrearUsuario, frmDatosDespacho, frmEditarCliente, frmEditarEstado,frmModificarDatos, frmDireccion
from django.shortcuts import get_object_or_404, redirect, render
from .models import Detallepedido, MiCarrito, Producto,Marca,TipoProd,Pedido,Estado, Direccion
from django.contrib import messages
from django.contrib.auth.forms import PasswordChangeForm, SetPasswordForm
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.hashers import check_password
from django.http import HttpResponseRedirect
from rest_framework import viewsets
from .serializers import srlzProducto


class ProductoViewSet(viewsets.ModelViewSet):
    queryset=Producto.objects.all()
    serializer_class=srlzProducto
# Create your views here.

def home(request):
    productos=Producto.objects.filter(es_destacado=True)
    contexto={
        "productos":productos
    }
    return render(request,"app_lmgas/home.html",contexto)
    
def Registro(request):
    form=frmCrearUsuario(request.POST or None)
    contexto={
        "formulario":form
    }

    if (request.method=="POST"):
        form=frmCrearUsuario(data=request.POST)
        if form.is_valid():
            form.save()
            return redirect(to="RegistroExitoso")
        contexto={
        "formulario":form
        }

    return render(request,"app_lmgas/Registro.html",contexto)

def AgregarProducto(request,id):
    prod=get_object_or_404(Producto,id=id)
    carrito=[]
    contexto={
        "prod":prod
    }

    return render(request,"app_lmgas/AgregarProducto.html",contexto)

def CambiarContraseña(request):
    if request.method=="POST":
        
        form = PasswordChangeForm(request.user, request.POST)
        old_password=request.POST.get("old_password")
        new_password1=request.POST.get("new_password1")
        new_password2=request.POST.get("new_password2")
        if not check_password(old_password, request.user.password):
            messages.error(request,"Su contraseña actual no coincide")
            return redirect(to=CambiarContraseña)
        if new_password1!=new_password2:
            messages.error(request,"Las contraseñas no coinciden")
            return redirect(to=CambiarContraseña)
        if form.is_valid():
            update_session_auth_hash(request, form.save())
            messages.success(request,"Su contraseña fue actualizada exitosamente")
            return redirect(to=MiCuenta)
    form=PasswordChangeForm(request.user)
    contexto={
    "form":form
    }
    return render(request, "app_lmgas/CambiarContraseña.html",contexto)

def AgregarProductoAdmin(request):
    form=FrmProducto(request.POST or None)
    
    contexto={
        "formulario":form
    }

    if request.method=="POST":
        form=FrmProducto(data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request,"Producto agregado exitosamente")
            return redirect(to=ListaProductos)
    
    return render(request, "app_lmgas/AgregarProductoAdmin.html",contexto)

def Carrito(request):
    total = 0
    contexto={}
    if request.user.is_authenticated:
        productos=MiCarrito.objects.filter(usuario=request.user.username)
        for producto in productos:
            total = total +producto.obtenerTotal()

        contexto={
            "productos":productos,
            "total":total
        }
    return render(request,"app_lmgas/Carrito.html",contexto)    

def AgregarCarrito(request,id):

    if request.user.is_authenticated:
        prod=get_object_or_404(Producto,id=id)
        car=MiCarrito()
        car.usuario= request.user.username
        car.id_producto=prod
        car.cantidad=1
        productos = MiCarrito.objects.filter(usuario=car.usuario)
        for producto in productos:
            if producto.id_producto.id==prod.id:
                car=get_object_or_404(MiCarrito,usuario=producto.usuario,id=producto.id)
                car.cantidad = car.cantidad+1
                car.save()
                messages.success(request,"Se ha aumentado en 1 la cantidad de este producto en su carrito ")
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        car.save()
        messages.success(request,"Producto agregado exitosamente, revisa tu carrito de compras.") 
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def AumentarCarrito(request,id):
    if request.user.is_authenticated:
        car=get_object_or_404(MiCarrito,id=id)
        car.cantidad= car.cantidad+1
        car.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def DisminuirCarrito(request,id):
    if request.user.is_authenticated:
        car=get_object_or_404(MiCarrito,id=id)
        car.cantidad= car.cantidad-1
        if car.cantidad<1:
            car.delete()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        car.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def EliminarCarrito(request,id):
    if request.user.is_authenticated:
        car=get_object_or_404(MiCarrito,id=id)
        car.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))         

def Cilindros(request):
    lista=Producto.objects.filter(tipo="1")
    
    contexto={
        "lista":lista
    }
    return render(request,"app_lmgas/Cilindros.html",contexto)

def Contacto(request):
    return render(request,"app_lmgas/Contacto.html")

def RegistroExitoso(request):
    return render(request,"app_lmgas/RegistroExitoso.html")

def Despacho(request):
    total=0
    pedido=Pedido()
    form=frmDatosDespacho(request.POST or None)
    if request.user.is_authenticated:
        pedidos=Pedido.objects.filter(rut=request.user.id)
        for p in pedidos:
            if p.activo:
                pedido=get_object_or_404(Pedido,id_pedido=p.id_pedido)
                form=frmDatosDespacho(instance=pedido)
                break

        # pedido.id_metodo=1
        pedido.rut=request.user
        pedido.valor_despacho=5000
        car=MiCarrito.objects.filter(usuario=request.user.username)
        for c in car:
            total = total + c.obtenerTotal()
        if request.method=="POST":
            form=frmDatosDespacho(data=request.POST,instance=pedido)
            if form.is_valid():
                form.save()
                return redirect(to=FinalizarCompra)
    contexto={
        "car":car,
        "total":total,
        "form":form
    }


    return render(request,"app_lmgas/Despacho.html",contexto)            

def ListaProductos(request):
    listaProductos=Producto.objects.all()
    contexto={
        "listaProductos":listaProductos
    }
    return render(request,"app_lmgas/ListaProductos.html",contexto)

def DetallePedido(request,id):
    pedido=get_object_or_404(Pedido,id_pedido=id)
    detalles=Detallepedido.objects.filter(id_pedido=id)

    contexto={
        "pedido":pedido,
        "detalles":detalles
    }
    return render(request,"app_lmgas/DetallePedido.html",contexto)

def MisPedidos(request):
    if request.user.is_authenticated:
        mispedidos=Pedido.objects.filter(rut=request.user.id)
        contexto={
            "mispedidos":mispedidos
        }
    return render(request,"app_lmgas/MisPedidos.html",contexto)

def EditarProducto(request,id):
    prod=get_object_or_404(Producto,id=id)
    form=FrmProducto(instance=prod)
    contexto={
        "formulario":form
    }

    if request.method=="POST":
        form=FrmProducto(data=request.POST,files=request.FILES,instance=prod)
        if form.is_valid():
            prod_buscado=Producto.objects.get(id=prod.id)
            datos_form=form.cleaned_data
            prod_buscado.marca=datos_form.get("marca")
            prod_buscado.tipo=datos_form.get("tipo")
            prod_buscado.valor=datos_form.get("valor")
            prod_buscado.stock=datos_form.get("stock")
            prod_buscado.descripcion=datos_form.get("descripcion")
            prod_buscado.es_destacado=datos_form.get("es_destacado")
            prod_buscado.imagen=datos_form.get("imagen")
            prod_buscado.save()
                
            messages.success(request,"Producto modificado exitosamente")
            return redirect(to=ListaProductos)

    return render(request,"app_lmgas/EditarProducto.html",contexto)

def EliminarProducto(request,id):
    prod=get_object_or_404(Producto,id=id)
    contexto={
        "prod":prod
    }
    return render(request,"app_lmgas/EliminarProducto.html",contexto)

def EliminarProductoSeguro(request,id):
    prod=get_object_or_404(Producto,id=id)
    prod.delete()
    return redirect(to=ListaProductos)      

def AdminPedidos(request):
    listaPedidos=Pedido.objects.all()
    estados=Estado.objects.all()

    contexto={
        "listaPedidos":listaPedidos,
        "estados":estados
    }

    return render(request,"app_lmgas/AdminPedidos.html",contexto)

def Distribuidores(request):
    return render(request,"app_lmgas/Distribuidores.html")

def Nosotros(request):
    return render(request,"app_lmgas/Nosotros.html") 

def Estanques(request):

    lista=Producto.objects.filter(tipo="2")
    contexto={
        "lista":lista
    }
    return render(request,"app_lmgas/Estanques.html",contexto) 

def MetodoDePago(request):
    return render(request,"app_lmgas/MetodoDePago.html")

def FinalizarCompra(request):
    contexto={}
    total=0
    totalfinal=0
    if request.user.is_authenticated:
        car=MiCarrito.objects.filter(usuario=request.user.username)
        pedido=get_object_or_404(Pedido,rut=request.user.id,activo=True)
        contexto={}
        for c in car:
            total=total + c.obtenerTotal()
        totalfinal=total+pedido.valor_despacho  
        user=request.user
        contexto={"user":user,"pedido":pedido,"car":car,"total":total,"totalfinal":totalfinal}

    return render(request,"app_lmgas/FinalizarCompra.html",contexto)

def TerminarCompra(request):
    if request.user.is_authenticated:
        car=MiCarrito.objects.filter(usuario=request.user.username)
        pedido=get_object_or_404(Pedido,rut=request.user.id,activo=True)
        for c in car:
            detalle =Detallepedido()
            prod=get_object_or_404(Producto,id=c.id_producto.id)
            detalle.id_pedido=pedido
            detalle.id_producto=c.id_producto
            detalle.cantidad=c.cantidad
            detalle.save()
            prod.stock = prod.stock - c.cantidad
            prod.save()
        car.delete()
        pedido.activo=False
        pedido.save()
        return redirect(to=Gracias)

    return redirect(to=FinalizarCompra)

def AdminClientes(request):
    listado=User.objects.all()
    contexto={
        "listado":listado
    }
    return render(request,"app_lmgas/AdminClientes.html",contexto) 

def EditarCliente(request,id):
    cliente=get_object_or_404(User,id=id)
    form=frmEditarCliente(instance=cliente)
    contexto={
        "form":form
    }

    if request.method=="POST":
        form=frmEditarCliente(data=request.POST,instance=cliente)
        if form.is_valid():
            form.save()
            messages.success(request,"Cliente modificado exitosamente")
        return redirect(to=AdminClientes)    
    return render(request,"app_lmgas/EditarCliente.html",contexto)

def MiCuenta(request):
    if request.user.is_authenticated:
        user=request.user
        direcciones=Direccion.objects.filter(id_cliente_id=request.user.id)
        contexto={
            "user":user,
            "direcciones":direcciones
        }
    return render(request,"app_lmgas/MiCuenta.html",contexto)

def ModificarDatos(request,id):
    user=get_object_or_404(User,id=id)
    form=frmModificarDatos(instance=user)
    contexto={
        "form":form
    }
    if request.method=="POST":
        form=frmModificarDatos(data=request.POST,instance=user)
        if not form.data['first_name'] or not form.data['last_name'] or not form.data['email']:
            messages.error(request,"Por favor rellene todos los campos del formulario")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        if form.is_valid():
            form.save()
            messages.success(request,"Su cuenta ha sido modificada exitosamente")
            return redirect(to=MiCuenta)
    return render(request,"app_lmgas/ModificarDatos.html", contexto)

def ModificarDatosAdmin(request,id):
    user=get_object_or_404(User,id=id)
    form=frmModificarDatos(instance=user)
    contexto={
        "form":form
    }
    if request.method=="POST":
        form=frmModificarDatos(data=request.POST,instance=user)
        if not form.data['first_name'] or not form.data['last_name'] or not form.data['email']:
            messages.error(request,"Por favor rellene todos los campos del formulario")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        if form.is_valid():
            form.save()
            messages.success(request,"Su cuenta ha sido modificada exitosamente")
            return redirect(to=Admin)
    return render(request,"app_lmgas/ModificarDatosAdmin.html", contexto)

def EditarPedido(request,id):
    pedido=get_object_or_404(Pedido,id_pedido=id)
    form=frmEditarEstado(instance=pedido)
    contexto={
        "form":form
    }

    if request.method=="POST":
        form=frmEditarEstado(data=request.POST,instance=pedido)
        if form.is_valid():
            form.save()
        return redirect(to=AdminPedidos)    

    return render(request,"app_lmgas/EditarPedido.html",contexto)

def Admin(request):
    if request.user.is_authenticated:
        user=request.user
        direcciones=Direccion.objects.filter(id_cliente_id=request.user.id)
        contexto={
            "user":user,
            "direcciones":direcciones
        }
    return render(request,"app_lmgas/Admin.html",contexto)

def Base(request):
    cant = MiCarrito.objects.filter(usuario=request.user.username).count()
    contexto={
            "cant":cant
        }
    return render(request,"app_lmgas/Base.html",contexto)

def AgregarDireccion(request):
    form=frmDireccion(request.POST or None)
    
    contexto={
        "formulario":form
    }

    if request.method=="POST":
        form=frmDireccion(data=request.POST, files=request.FILES)
        if form.is_valid():
            direccion =Direccion()
            direccion.id_cliente=request.user
            direccion.alias=form.cleaned_data.get("alias")
            direccion.id_region=form.cleaned_data.get("id_region")
            direccion.numero=form.cleaned_data.get("numero")
            direccion.calle=form.cleaned_data.get("calle")
            direccion.comuna=form.cleaned_data.get("comuna")
            direccion.telefono=form.cleaned_data.get("telefono")
            direccion.save()
            messages.success(request,"Direccion agregada exitosamente")
            return redirect(to=MiCuenta)
    
    return render(request, "app_lmgas/AgregarDireccion.html",contexto)

def EditarDireccion(request,id):
    dir=get_object_or_404(Direccion,id_direccion=id)
    form=frmDireccion(instance=dir)
    contexto={
        "formulario":form
    }

    if request.method=="POST":
        form=frmDireccion(data=request.POST,instance=dir)
        if form.is_valid():
            dir_buscada=Direccion.objects.get(id_direccion=dir.id_direccion)
            datos_form=form.cleaned_data
            dir_buscada.id_cliente=request.user
            dir_buscada.alias=datos_form.get("alias")
            dir_buscada.id_region=datos_form.get("id_region")
            dir_buscada.numero=datos_form.get("numero")
            dir_buscada.calle=datos_form.get("calle")
            dir_buscada.comuna=datos_form.get("comuna")
            dir_buscada.telefono=datos_form.get("telefono")
            dir_buscada.save()
                
            messages.success(request,"Direccion modificada exitosamente")
            return redirect(to=MiCuenta)

    return render(request,"app_lmgas/EditarDireccion.html",contexto)

def EliminarDireccion(request,id):
    dir=get_object_or_404(Direccion,id_direccion=id)
    dir.delete()
    messages.success(request,"Direccion eliminada exitosamente")
    return redirect(to=MiCuenta)


def AgregarDireccion2(request):
    form=frmDireccion(request.POST or None)
    
    contexto={
        "formulario":form
    }

    if request.method=="POST":
        form=frmDireccion(data=request.POST, files=request.FILES)
        if form.is_valid():
            direccion =Direccion()
            direccion.id_cliente=request.user
            direccion.alias=form.cleaned_data.get("alias")
            direccion.id_region=form.cleaned_data.get("id_region")
            direccion.numero=form.cleaned_data.get("numero")
            direccion.calle=form.cleaned_data.get("calle")
            direccion.comuna=form.cleaned_data.get("comuna")
            direccion.telefono=form.cleaned_data.get("telefono")
            direccion.save()
            messages.success(request,"Direccion agregada exitosamente")
            return redirect(to=Despacho)
    
    return render(request, "app_lmgas/AgregarDireccion.html",contexto)

def Gracias(request):
    return render(request,"app_lmgas/Gracias.html")